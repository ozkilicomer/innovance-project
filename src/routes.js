import React from 'react';
import Home from './pages/Home';
import Detail from './pages/Detail';
import Favorites from './pages/Favorites';
import { ListDetails, Heart, Home2 } from 'tabler-icons-react';

const routes = [
  {
    name: 'home',
    path: '/',
    component: Home,
    exact: true,
    icon: <Home2 size={16} />,
    color: 'blue',
  },
  {
    name: 'detail',
    path: '/detail',
    component: Detail,
    icon: <ListDetails size={16} />,
    color: 'teal',
  },
  {
    name: 'favorites',
    path: '/favorites',
    component: Favorites,
    icon: <Heart fill="red" size={16} />,
    color: 'violet',
  },
];

export default routes;
