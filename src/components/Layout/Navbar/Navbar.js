import {
  Avatar,
  Box,
  Navbar,
  Text,
  ThemeIcon,
  UnstyledButton,
  useMantineTheme,
} from '@mantine/core';
import React from 'react';
import { useHistory } from 'react-router-dom';
import routes from '../../../routes';
import { Group } from '@mantine/core';
import { useTranslation } from 'react-i18next';

const Footer = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const theme = useMantineTheme();

  const mapRoutes = () => {
    let routesTr = [...routes];
    routesTr = routesTr.map((val) => {
      return {
        ...val,
        name: t(val.name),
      };
    });
    return routesTr;
  };

  const routesTranslations = mapRoutes();
  return (
    <>
      <Navbar.Section grow mt="md">
        <Group direction="column">
          {routesTranslations.map((route) => {
            const { path } = route;
            return (
              <UnstyledButton
                sx={(theme) => ({
                  display: 'block',
                  width: '100%',
                  padding: theme.spacing.xs,
                  borderRadius: theme.radius.sm,
                  color:
                    theme.colorScheme === 'dark'
                      ? theme.colors.dark[0]
                      : theme.black,

                  '&:hover': {
                    backgroundColor:
                      theme.colorScheme === 'dark'
                        ? theme.colors.dark[6]
                        : theme.colors.gray[0],
                  },
                })}
                key={path}
                onClick={(e) => {
                  e.preventDefault();
                  history.push(path);
                }}
              >
                <Group>
                  <ThemeIcon color={route.color} variant="light">
                    {route.icon}
                  </ThemeIcon>

                  <Text size="sm">{route.name}</Text>
                </Group>
              </UnstyledButton>
            );
          })}
        </Group>
      </Navbar.Section>
      <Navbar.Section>
        <Box
          sx={{
            paddingTop: theme.spacing.sm,
            borderTop: `1px solid ${
              theme.colorScheme === 'dark'
                ? theme.colors.dark[4]
                : theme.colors.gray[2]
            }`,
          }}
        >
          <UnstyledButton
            sx={{
              display: 'block',
              width: '100%',
              padding: theme.spacing.xs,
              borderRadius: theme.radius.sm,
              color:
                theme.colorScheme === 'dark'
                  ? theme.colors.dark[0]
                  : theme.black,

              '&:hover': {
                backgroundColor:
                  theme.colorScheme === 'dark'
                    ? theme.colors.dark[6]
                    : theme.colors.gray[0],
              },
            }}
          >
            <Group>
              <Avatar
                src="https://images.unsplash.com/photo-1508214751196-bcfd4ca60f91?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=255&q=80"
                radius="xl"
              />
              <Box sx={{ flex: 1 }}>
                <Text size="sm" weight={500}>
                  User
                </Text>
                <Text color="dimmed" size="xs">
                  userinnovance@gmail.com
                </Text>
              </Box>
            </Group>
          </UnstyledButton>
        </Box>
      </Navbar.Section>
    </>
  );
};

export default Footer;
