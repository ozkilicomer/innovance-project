import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
  Badge,
  Button,
  Card,
  Grid,
  Group,
  Image,
  LoadingOverlay,
  Pagination,
  Text,
  Title,
  useMantineTheme,
} from '@mantine/core';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { Heart } from 'tabler-icons-react';
import { useDispatch, useSelector } from 'react-redux';
import { favoriteActions } from '../../app/slices/FavoriteSlices';

const HomePage = () => {
  const { t } = useTranslation();
  const theme = useMantineTheme();
  const history = useHistory();
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.favorite.favorites);
  const secondaryColor =
    theme.colorScheme === 'dark' ? theme.colors.dark[1] : theme.colors.gray[7];
  const [trendings, setTrendings] = useState({
    isLoading: false,
    items: [],
    selected: null,
    page: 1,
    total_count: 0,
    total_pages: 0,
  });

  useEffect(() => {
    fetchTrendings();
  }, [trendings.page]);

  const fetchTrendings = () => {
    setTrendings((prevState) => ({ ...prevState, isLoading: true }));
    const params = {
      api_key: process.env.REACT_APP_API_KEY,
      language: localStorage.getItem('language'),
      page: trendings.page,
    };
    axios
      .get(`https://api.themoviedb.org/3/movie/now_playing`, { params })
      .then((response) => {
        setTrendings((prevState) => ({
          ...prevState,
          isLoading: false,
          items: response.data.results,
          total_count: response.data.total_results,
          total_pages: response.data.total_pages,
        }));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const voteAvreageBadge = (avreage) => {
    if (avreage >= 8.5) {
      return (
        <Badge variant="light" color="green" text={avreage}>
          {t('veryGood')}
        </Badge>
      );
    } else if (avreage >= 7.5) {
      return (
        <Badge variant="light" color="yellow" text={avreage}>
          {t('good')}
        </Badge>
      );
    } else if (avreage >= 5.5) {
      return (
        <Badge variant="light" color="orange" text={avreage}>
          {t('average')}
        </Badge>
      );
    } else if (avreage >= 3.5) {
      return (
        <Badge variant="light" color="pink" text={avreage}>
          {t('bad')}
        </Badge>
      );
    } else {
      return (
        <Badge variant="light" color="red" text={avreage}>
          {t('veryBad')}
        </Badge>
      );
    }
  };

  const addFav = (item) => {
    dispatch(favoriteActions.addFavorite(item));
    setTrendings((prevState) => ({ ...prevState, selected: {} }));
  };

  return (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('trendings')}
      </Title>
      <Grid gutter="xl">
        <LoadingOverlay visible={trendings.isLoading} />
        {(trendings.items || []).map((item, index) => (
          <Grid.Col xs={12} md={6} lg={4} span={2} key={index}>
            <Card shadow="sm" p="lg">
              <Card.Section>
                <Image
                  src={`http://image.tmdb.org/t/p/w500/${item.poster_path}`}
                  height={250}
                  alt={item.title}
                  fit="contain"
                />
              </Card.Section>

              <Group
                position="apart"
                style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
              >
                <Text weight={500}>{item?.original_title ?? ''}</Text>
                {voteAvreageBadge(item?.vote_average ?? 0)}
              </Group>

              <Text
                lineClamp={3}
                size="md"
                style={{ color: secondaryColor, lineHeight: 1.5 }}
              >
                {item?.overview
                  ? item.overview
                  : 'lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quidem. Quasi, quisquam.lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quidem. Quasi, quisquam.lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam, quidem. Quasi, quisquam.'}
              </Text>
              <Grid>
                <Grid.Col span={6}>
                  <Button
                    variant="light"
                    color="blue"
                    fullWidth
                    style={{ marginTop: 14 }}
                    onClick={() => {
                      history.push(`/detail/${item.id}`, { item });
                    }}
                  >
                    {t('detail')}
                  </Button>
                </Grid.Col>
                <Grid.Col span={6}>
                  <Button
                    variant="light"
                    color="pink"
                    fullWidth
                    style={{ marginTop: 14 }}
                    onClick={() => {
                      if (favorites.find((fav) => fav.id === item.id)) {
                        dispatch(favoriteActions.removeFavorite(item));
                      } else {
                        addFav(item);
                      }
                    }}
                  >
                    <Heart
                      fill={
                        favorites.some((itemFav) => itemFav.id === item.id)
                          ? 'red'
                          : 'none'
                      }
                      color="red"
                      size={16}
                    />
                  </Button>
                </Grid.Col>
              </Grid>
            </Card>
          </Grid.Col>
        ))}
      </Grid>
      <Pagination
        sx={{ display: 'flex', justifyContent: 'center', marginTop: '2rem' }}
        page={trendings.page}
        onChange={(value) => {
          setTrendings((prevState) => ({ ...prevState, page: value }));
        }}
        total={10}
      />
    </>
  );
};

export default HomePage;
