import styled from 'styled-components';

import { v } from '../../styles/variables';

export const SDetail = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: calc(100vh - ${v.headerHeight} - ${v.lgSpacing} * 2);
`;
