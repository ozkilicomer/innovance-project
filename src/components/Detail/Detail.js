import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import {
  Paper,
  Title,
  Text,
  Grid,
  Image,
  Chips,
  Chip,
  Blockquote,
  Box,
  Group,
  Avatar,
  Input,
  Button,
  ThemeIcon,
} from '@mantine/core';
import { useTranslation } from 'react-i18next';
import { CircleCheck, Search } from 'tabler-icons-react';

const Detail = () => {
  const { t } = useTranslation();
  const history = useHistory();
  const [detail, setDetail] = useState({
    isLoading: false,
    selected: null,
  });
  const [searchText, setSearchText] = useState('');
  const [searchData, setSearchData] = useState([]);
  const id = history.location.pathname.split('/')[2];

  useEffect(() => {
    if (id === undefined) {
      return;
    } else {
      fetchDetail();
    }
  }, []);

  const fetchDetail = (Id) => {
    setDetail((prevState) => ({ ...prevState, isLoading: true }));
    const params = {
      api_key: process.env.REACT_APP_API_KEY,
      language: localStorage.getItem('language'),
    };
    axios
      .get(`https://api.themoviedb.org/3/movie/${id ?? Id}`, {
        params,
      })
      .then((response) => {
        setDetail((prevState) => ({
          ...prevState,
          isLoading: false,
          selected: response.data,
        }));
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const fetchSearchMovie = () => {
    const params = {
      api_key: process.env.REACT_APP_API_KEY,
      language: localStorage.getItem('language'),
    };
    axios
      .get(`https://api.themoviedb.org/3/search/movie?&query=${searchText}`, {
        params,
      })
      .then((response) => {
        setSearchData(response?.data?.results);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return id === undefined ? (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('search')}
      </Title>
      <Grid sx={{ marginBottom: '2rem' }}>
        <Grid.Col span={10}>
          <Input
            icon={<Search size={16} />}
            placeholder={t('search')}
            rightSectionWidth={70}
            styles={{ rightSection: { pointerEvents: 'none' } }}
            onChange={(event) => {
              setSearchText(event.target.value);
            }}
          />
        </Grid.Col>
        <Grid.Col span={2}>
          <Button
            variant="light"
            color="blue"
            fullWidth
            disabled={searchText === ''}
            onClick={() => {
              fetchSearchMovie();
            }}
          >
            {t('search')}
          </Button>
        </Grid.Col>
      </Grid>
      <Paper shadow="md" radius="md" p="md">
        {searchData.map((item, index) => {
          return (
            <Grid sx={{ alignItems: 'center' }} key={index}>
              <Grid.Col span={2}>
                <ThemeIcon color="teal" size={24} radius="xl">
                  <CircleCheck size={16} />
                </ThemeIcon>
              </Grid.Col>
              <Grid.Col span={8}>{item.title}</Grid.Col>
              <Grid.Col span={2}>
                <Button
                  variant="light"
                  color="blue"
                  fullWidth
                  onClick={() => {
                    history.push(`/detail/${item.id}`);
                    fetchDetail(item.id);
                  }}
                >
                  {t('detail')}
                </Button>
              </Grid.Col>
            </Grid>
          );
        })}
      </Paper>
    </>
  ) : (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('detail')}
      </Title>
      <Paper shadow="md" radius="md" p="md">
        <Grid>
          <Grid.Col span={4}>
            <Image
              src={`http://image.tmdb.org/t/p/w500/${detail?.selected?.poster_path}`}
              height={500}
              alt={detail?.selected?.title}
            />
          </Grid.Col>
          <Grid.Col span={8}>
            <Chips sx={{ marginBottom: '2rem' }} spacing="xl" size="md">
              {detail?.selected?.genres?.map((genre, index) => (
                <Chip key={index} value={genre.id}>
                  {genre.name}
                </Chip>
              ))}
            </Chips>
            <Title sx={{ marginBottom: '2rem' }} order={1}>
              {detail?.selected?.title ?? ''}
            </Title>
            <Text sx={{ marginBottom: '2rem' }}>
              {detail?.selected?.overview ?? ''}
            </Text>
            <Title order={2}>{t('productions')}</Title>
            <Grid>
              {detail?.selected?.production_companies?.map((company, index) => (
                <Grid.Col span={6} key={index}>
                  <Box
                    sx={(theme) => ({
                      display: 'block',
                      width: '100%',
                      padding: theme.spacing.xs,
                      borderRadius: theme.radius.sm,
                      color:
                        theme.colorScheme === 'dark'
                          ? theme.colors.dark[0]
                          : theme.black,
                      backgroundColor:
                        theme.colorScheme === 'dark'
                          ? theme.colors.dark[6]
                          : theme.colors.gray[0],
                      marginBottom: '1rem',
                    })}
                  >
                    <Group>
                      <Avatar
                        src={`http://image.tmdb.org/t/p/w500/${company?.logo_path}`}
                        radius="xl"
                      />
                      <Box sx={{ flex: 1 }}>
                        <Text size="sm" weight={500}>
                          {company.name}
                        </Text>
                        <Text color="dimmed" size="xs">
                          {company.origin_country}
                        </Text>
                      </Box>
                    </Group>
                  </Box>
                </Grid.Col>
              ))}
            </Grid>
          </Grid.Col>
        </Grid>
        <Blockquote cite={`-${detail?.selected?.title ?? ''}`}>
          {detail?.selected?.tagline ?? ''}.
        </Blockquote>
      </Paper>
    </>
  );
};

export default Detail;
