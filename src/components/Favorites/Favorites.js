import { Button, Table, Title } from '@mantine/core';
import { useTranslation } from 'react-i18next';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { favoriteActions } from '../../app/slices/FavoriteSlices';

const Favorites = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const favorites = useSelector((state) => state.favorite.favorites);
  const ths = (
    <tr>
      <th>{t('movieName')}</th>
      <th>{t('removeFav')}</th>
    </tr>
  );

  const rows = favorites.map((element) => (
    <tr key={element.id}>
      <td>{element.title}</td>
      <td>
        <Button
          variant="light"
          color="blue"
          fullWidth
          style={{ marginTop: 14 }}
          onClick={() => {
            dispatch(favoriteActions.removeFavorite(element));
          }}
        >
          {t('removeFav')}
        </Button>
      </td>
    </tr>
  ));
  return (
    <>
      <Title sx={{ marginBottom: '1rem' }} order={1}>
        {t('favorites')}
      </Title>
      <Table>
        <thead>{ths}</thead>
        <tbody>{rows}</tbody>
      </Table>
    </>
  );
};

export default Favorites;
