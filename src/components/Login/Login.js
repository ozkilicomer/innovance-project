import React, { useState, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Auth from '../../libraries/Auth';
import BoxAuth from '../../libraries/Box';
import { useTranslation } from 'react-i18next';
import { SLogin, LoginBox, LoginBoxWrapper } from './styles';
import { loginValidation } from '../../app/slices/AuthSlice';
import { TextInput, Checkbox, Button, Group, Box } from '@mantine/core';
import { useForm } from '@mantine/form';
import ThemeContext from '../../app/context/ThemeContext';
import { lightTheme, darkTheme } from '../../styles/theme';

const Login = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);
  const theme = useContext(ThemeContext);
  const form = useForm({
    initialValues: {
      email: '',
      password: '',
      termsOfService: false,
    },

    validate: {
      email: (value) => (/^\S+@\S+$/.test(value) ? null : 'Invalid email'),
    },
  });
  const [loginForm, setLoginForm] = useState({
    loading: false,
    isRedirect: false,
  });

  const onSubmit = () => {
    setLoginForm({ ...loginForm, loading: true });
    Auth.rehydratereToken();
    dispatch(loginValidation(true));
    setLoginForm({ isRedirect: true, loading: false });
  };

  if (loginForm.isRedirect || (BoxAuth.auth.isValid && auth?.isValid)) {
    return <Redirect exact push to="/" />;
  }

  return (
    <SLogin>
      <LoginBox>
        <LoginBoxWrapper>
          <Box
            sx={{
              padding: '2.5rem',
              maxWidth: 300,
              backgroundColor: theme === 'light' ? lightTheme.bg : darkTheme.bg,
            }}
            mx="auto"
          >
            <form onSubmit={(values) => onSubmit(values)}>
              <TextInput
                required
                label="Email"
                placeholder="your@email.com"
                {...form.getInputProps('email')}
              />
              <TextInput
                type="password"
                required
                label="Password"
                placeholder=""
                {...form.getInputProps('password')}
              />

              <Checkbox
                mt="md"
                label="I agree to sell my privacy"
                {...form.getInputProps('termsOfService', { type: 'checkbox' })}
              />

              <Group position="right" mt="md">
                <Button type="submit">{t('login')}</Button>
              </Group>
            </form>
          </Box>
        </LoginBoxWrapper>
      </LoginBox>
    </SLogin>
  );
};

export default Login;
