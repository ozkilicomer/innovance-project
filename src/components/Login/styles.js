import styled from 'styled-components';

export const SLogin = styled.div`
  height: 100vh;
  width: 100vw;
`;

export const LoginBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
`;

export const LoginBoxWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
