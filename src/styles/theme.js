export const lightTheme = {
  bg: 'rgb(252, 252, 252)',
};

export const darkTheme = {
  bg: '#5C5F66',
};
