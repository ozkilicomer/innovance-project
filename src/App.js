import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import { Redirect, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import i18n from './i18n';
import { GlobalStyles } from './styles/globalStyles';
import { darkTheme, lightTheme } from './styles/theme';
import Login from './pages/Login';
import TheLayout from './containers/TheLayout';
import Auth from './libraries/Auth';
import { ThemeProviderContextTheme } from './app/context/ThemeContext';
import { MantineProvider } from '@mantine/core';

const App = () => {
  const { theme } = useSelector((state) => state.ui.theme);
  const currentTheme = theme === 'light' ? lightTheme : darkTheme;
  const currentLang = useSelector((state) => state.lang.currentLang);
  localStorage.setItem('language', currentLang ?? 'tr');
  const loading = () => <div>loading...</div>;
  Auth.rehydratereToken();
  return (
    <I18nextProvider i18n={i18n}>
      <MantineProvider theme={{ colorScheme: theme }}>
        <ThemeProviderContextTheme>
          <ThemeProvider theme={currentTheme}>
            <GlobalStyles />
            <Helmet>
              <title>Innovance Project</title>
              <link rel="preconnect" href="https://fonts.googleapis.com" />
              <link
                rel="preconnect"
                href="https://fonts.gstatic.com"
                crossOrigin
              />
              <link
                href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap"
                rel="stylesheet"
              />
            </Helmet>
            <>
              <Suspense fallback={loading()}>
                <Switch>
                  <Route
                    exact
                    path="/login"
                    name="login"
                    render={(props) => <Login {...props} />}
                  />
                  <Route
                    path="/"
                    name="home"
                    render={(props) => <TheLayout {...props} />}
                  />
                  <Redirect from="/" to="/home" />
                </Switch>
              </Suspense>
            </>
          </ThemeProvider>
        </ThemeProviderContextTheme>
      </MantineProvider>
    </I18nextProvider>
  );
};

export default App;
