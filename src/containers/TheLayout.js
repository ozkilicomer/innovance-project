import React from 'react';
import Box from '../libraries/Box';
import HeaderMain from '../components/Layout/Header/Header';
import NavbarMain from '../components/Layout/Navbar/Navbar';
import { Redirect } from 'react-router-dom';
import Content from './Content';
import { useSelector } from 'react-redux';
import { AppShell, Navbar, Header } from '@mantine/core';
import { lightTheme, darkTheme } from '../styles/theme';

const TheLayout = () => {
  const auth = useSelector((state) => state.auth);
  const theme = useSelector((state) => state.ui.theme);
  if (Box.auth.isValid && auth?.isValid) {
    return (
      <AppShell
        padding="md"
        navbar={
          <Navbar
            styles={{
              root: {
                backgroundColor:
                  theme === 'light' ? lightTheme.bg : darkTheme.bg,
              },
            }}
            width={{ base: 300 }}
            p="xs"
          >
            <NavbarMain />
          </Navbar>
        }
        header={
          <Header sx={{ padding: '0px !important' }} height={60} p="xs">
            <HeaderMain />
          </Header>
        }
        styles={() => ({
          main: {
            backgroundColor: theme === 'light' ? lightTheme.bg : darkTheme.bg,
          },
        })}
      >
        <Content />
      </AppShell>
    );
  }
  return <Redirect exact push to="/login" />;
};

export default TheLayout;
